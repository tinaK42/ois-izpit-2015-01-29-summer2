var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
 //9.
app.get('/api/prijava', function(req, res) {
	var username = req.query.uporabniskoIme;
	var password = req.query.geslo;
	var uspesno = preveriSpomin(username, password) || preveriDatotekaStreznik(username, password);
	
	var napaka = "";
 	if (username == null || username.length == 0 || password == null || password.length == 0)
  		napaka = "Napačna zahteva.";
 	else if (!uspesno) {
		napaka = "Avtentikacija ni uspela.";
 }
	
	//od prej: res.send({status: "status", napaka: "Opis napake"});
	 res.send({status: uspesno, napaka: napaka});
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
 //8.
app.get('/prijava', function(req, res) {
	var username = req.query.uporabniskoIme;
	var password = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaOdjemalec(uporabniskoIme, geslo);
	
	var odgovor = "<html><head><title>" + (uspesno==true ? "Uspešno":"Napaka") + "</title></head></html>";
	//ce moras kej oblikvt: "</title><style>body {padding:10px; color:red; font-style:italic}</style></head>";
	odgovor = odgovor + "<body>";
	if (uspesno)
		odgovor += "<p>Uporabnik <b>" + uporabniskoIme + "</b> uspešno prijavljen v sistem!</p>";
	else
		odgovor += "<p>Uporabnik <b>" + uporabniskoIme + "</b> nima pravice prijave v sistem!</p>";
	
	// od prej: res.send("<html><title>Naslov strani</title><body><p>Uporabnik <b>Krneki</b> nima pravice prijave v sistem!</p></body></html>");
	res.send(odgovor);
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
 //5.
//var podatkiDatotekaStreznik = {}; -- tu je blu ze prej
var podatkiDatotekaStreznik = JSON.parse((fs.readFileSync(__dirname + "/public/podatki/uporabniki_streznik.json").toString()));

/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
 //6.
function preveriSpomin(uporabniskoIme, geslo) {
	for (i in podatkiSpomin) {
	var username = podatkiSpomin[i].split("/")[0];
	var password = podatkiSpomin[i].split("/")[1];
	
	if (username==uporabniskoIme && password==geslo)
		return true;
	}
	return false;
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
 //7.
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	for (i in podatkiDatotekaStreznik) {
		var username = podatkiDatotekaStreznik[i]["uporabnik"];
		var password = podatkiDatotekaStreznik[i]["geslo"];
		
		if (username==uporabniskoIme && password==geslo)
			return true;
	}
	
	return false;
}